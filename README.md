# CUB-Project-Android



## 簡介

這是一個Android app，用來顯示github用戶列表及詳細資料。

## 安裝

- [cub-demo.apk](https://gitlab.com/a0976266157/cub-project-android/-/blob/main/app/debug/cub-demo.apk)

## 第三方套件
- [Glide](https://github.com/bumptech/glide)
- [Retrofit](https://square.github.io/retrofit/)


## 開發環境

- Android studio Bumblebee 2021.1.1 Patch 3
- Gradle JDK 11

## 版本更新紀錄
### 版本 1.0.0（2023-04-11）
- 首次發布



