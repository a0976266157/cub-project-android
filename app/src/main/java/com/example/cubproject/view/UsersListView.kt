package com.example.cubproject.view

import com.example.cubproject.model.User

interface UsersListView  {
    fun updateList(users:List<User>)
}