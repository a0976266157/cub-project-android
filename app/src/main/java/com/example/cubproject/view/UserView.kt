package com.example.cubproject.view

import com.example.cubproject.model.User
import com.example.cubproject.model.UserDetail

interface UserView {
    fun setUser(user:UserDetail)
}