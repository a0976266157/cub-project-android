package com.example.cubproject

import android.util.Log
import com.example.cubproject.model.GithubService
import com.example.cubproject.model.UserModel
import com.example.cubproject.view.UserView
import com.example.cubproject.view.UsersListView

class UserListPresenter(private val view: UsersListView, private val model: UserModel) {
    fun getUsersList() {
        model.getUsersList(
            onSuccess = {
                view.updateList(it)
            }, onError = {
                Log.d("presenter", "Failed to load users")
            }
        )
    }
}

class UserDetailPresenter(private val view: UserView, private val model: UserModel) {
    fun getDetail(name: String) {
        model.getDetail(
            name,
            onSuccess = {
                view.setUser(it)
            }, onError = {
                Log.d("presenter", "Failed to get detail")
            }
        )

    }
}