package com.example.cubproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.cubproject.databinding.ActivityUserDetailBinding
import com.example.cubproject.model.UserDetail
import com.example.cubproject.model.UserModel
import com.example.cubproject.view.UserView

class UserDetailActivity : AppCompatActivity(), UserView {
    private lateinit var binding: ActivityUserDetailBinding
    private lateinit var presenter: UserDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.supportActionBar?.hide()
        binding = ActivityUserDetailBinding.inflate(layoutInflater)
        binding.closeBtn.setOnClickListener { finish() }
        val name = intent.extras?.get("name") as String
        presenter = UserDetailPresenter(this, UserModel())
        presenter.getDetail(name)
        setContentView(binding.root)
    }

    override fun setUser(user: UserDetail) {
        Glide.with(this)
            .load(user.avatar_url)
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_image)
            .circleCrop()
            .into(binding.avaImg)
        binding.nameTv.text = user.name
        Glide.with(this).load(user.bio).into(binding.bioImg)
        binding.loginTv.text = user.login
        binding.locationTv.text = user.location
        binding.linkTv.text = user.blog
    }
}