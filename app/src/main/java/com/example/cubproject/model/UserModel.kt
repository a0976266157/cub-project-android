package com.example.cubproject.model

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

data class User(
    val login: String,
    val id: Int,
    val node_id: String,
    val avatar_url: String,
    val gravatar_id: String,
    val url: String,
    val html_url: String,
    val followers_url: String,
    val following_url: String,
    val gists_url: String,
    val starred_url: String,
    val subscriptions_url: String,
    val organizations_url: String,
    val repos_url: String,
    val events_url: String,
    val received_events_url: String,
    val type: String,
    val site_admin: Boolean
)

data class UserDetail(
    val login: String,
    val id: Int,
    val node_id: String,
    val avatar_url: String,
    val gravatar_id: String,
    val url: String,
    val html_url: String,
    val followers_url: String,
    val following_url: String,
    val gists_url: String,
    val starred_url: String,
    val subscriptions_url: String,
    val organizations_url: String,
    val repos_url: String,
    val events_url: String,
    val received_events_url: String,
    val type: String,
    val site_admin: Boolean,
    val name: String?,
    val company: String?,
    val blog: String?,
    val location: String?,
    val email: String?,
    val hireable: Boolean?,
    val bio: String?,
    val twitter_username: String?,
    val public_repos: Int,
    val public_gists: Int,
    val followers: Int,
    val following: Int,
    val created_at: String,
    val updated_at: String
)

interface GithubService {
    @GET("users")
    fun getUsersList(): Call<List<User>>

    @GET("users/{username}")
    fun getUserDetail(
        @Path("username") name: String
    ): Call<UserDetail>
}

class UserModel {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val githubService = retrofit.create(GithubService::class.java)

    fun getUsersList(onSuccess: (List<User>) -> Unit, onError: (String) -> Unit) {
        githubService.getUsersList().enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                if (response.isSuccessful) {
                    val users = response.body() ?: emptyList()
                    onSuccess(users)
                } else {
                    onError("Failed to get list")
                }
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                onError("Failed to get list")
            }
        })
    }

    fun getDetail(name: String, onSuccess: (UserDetail) -> Unit, onError: (String) -> Unit) {
        githubService.getUserDetail(name).enqueue(object : Callback<UserDetail> {
            override fun onResponse(call: Call<UserDetail>, response: Response<UserDetail>) {
                if (response.isSuccessful) {
                    val users = response.body()
                    onSuccess(users)
                } else {
                    onError("Failed to get detail")
                }
            }

            override fun onFailure(call: Call<UserDetail>, t: Throwable) {
                onError("Failed to get detail")
            }
        })
    }
}