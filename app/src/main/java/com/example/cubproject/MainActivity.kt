package com.example.cubproject

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cubproject.databinding.ActivityMainBinding
import com.example.cubproject.model.User
import com.example.cubproject.model.UserModel
import com.example.cubproject.view.UsersListView

class MainActivity : AppCompatActivity(), UsersListView {
    private lateinit var binding: ActivityMainBinding
    private lateinit var mUserListAdapter: UserListAdapter
    private lateinit var presenter: UserListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "GitHub Users"
        binding = ActivityMainBinding.inflate(layoutInflater)
        mUserListAdapter = UserListAdapter(this, object : UserListAdapter.OnItemClick {
            override fun onClick(name: String) {
                val intent = Intent(this@MainActivity, UserDetailActivity::class.java)
                intent.putExtra("name", name)
                startActivity(intent)
            }
        })
        binding.userRv.adapter = mUserListAdapter
        val userModel = UserModel()
        presenter = UserListPresenter(this, userModel)
        presenter.getUsersList()
        setContentView(binding.root)
    }

    override fun updateList(users: List<User>) {
        mUserListAdapter.setData(users)
    }

    class UserListAdapter(private val context: Context, private val onClick: OnItemClick) :
        RecyclerView.Adapter<UserListAdapter.ViewHolder>() {
        private var users: List<User> = emptyList()

        interface OnItemClick {
            fun onClick(name: String)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val user = users[position]
            holder.nameTv.text = user.login
            Log.d("UserListAdapter", "avatarUrl: ${user.avatar_url}")
            Glide.with(context)
                .load(user.avatar_url)
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image)
                .circleCrop()
                .into(holder.avatarImg)
            if (user.site_admin) holder.badge.visibility = View.VISIBLE
            else holder.badge.visibility = View.GONE
            holder.itemView.setOnClickListener { onClick.onClick(user.login) }
        }

        override fun getItemCount(): Int {
            return users.size
        }

        fun setData(users: List<User>) {
            this.users = users
            notifyDataSetChanged()
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val nameTv: TextView = view.findViewById(R.id.login_tv)
            val avatarImg: ImageView = view.findViewById(R.id.ava_img)
            val badge: TextView = view.findViewById(R.id.title_tv)
        }
    }
}